using System.Data.SqlTypes;

namespace extractclass
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductLabel { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}