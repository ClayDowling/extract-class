using System.Collections.Generic;

namespace extractclass
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public List<CustomerSale> Purchases { get; } = new List<CustomerSale>();
    }
}