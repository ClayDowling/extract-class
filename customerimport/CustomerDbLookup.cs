using System.Linq;

namespace extractclass
{
    public static class CustomerDbLookup
    {
        public static CustomerContext Db = null;
        
        public static Customer Lookup(string email, string firstname, string lastname)
        {
            var cust = new Customer();
                        
            cust.Email = email;
            cust.FirstName = firstname;
            cust.LastName = lastname;

            Db ??= new CustomerContext();
            Db.Customer.Add(cust);
            Db.SaveChanges();

            return cust;
        }
    }
}