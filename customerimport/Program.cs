﻿using System;

namespace extractclass
{
    class Program
    {
        static void Main(string[] args)
        {
            SalesImporter importer = new SalesImporter();
            
            importer.ImportSales(args[0]);
            
            Console.WriteLine("Import finished");
        }
    }
}
