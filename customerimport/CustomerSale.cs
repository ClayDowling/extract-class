namespace extractclass
{
    public class CustomerSale
    {
        public string TransactionId { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public int Quantity { get; set; }

        public Customer SaleCustomer { get; set; }
        public Product SaleProduct { get; set; }
    }
}