using System;
using System.IO;
using System.Linq;
using System.Transactions;

namespace extractclass
{
    public class SalesImporter
    {

        public SalesImporter()
        {
            CustomerLookup = CustomerDbLookup.Lookup;
        }

        public Func<string, string, string, Customer> CustomerLookup;
        
        public void ImportSales(string salesfile)
        {
            string TxnId;
            string Firstname;
            string Lastname;
            string Email;
            string ProductCode;
            string Qty;
            string SalePrice;

            using StreamReader sr = new StreamReader(salesfile);
            using var db = new CustomerContext();
            Customer cust = new Customer();
            string currentLine;
            // currentLine will be null when the StreamReader reaches the end of file
            while((currentLine = sr.ReadLine()) != null)
            {
                var parts = currentLine.Split(',');
                TxnId = parts[0];
                Firstname = parts[1];
                Lastname = parts[2];
                Email = parts[3];
                ProductCode = parts[4];
                Qty = parts[5];
                SalePrice = parts[6];

                if (cust.Email != Email)
                {
                    cust = new Customer();
                        
                    cust.Email = Email;
                    cust.FirstName = Firstname;
                    cust.LastName = Lastname;

                    db.Customer.Add(cust);
                    db.SaveChanges();
                }

                Product prod = db.Product.First(p => p.ProductLabel == ProductCode);
                
                CustomerSale sale = new CustomerSale();
                sale.TransactionId = TxnId;
                sale.CustomerId = cust.CustomerId;
                sale.ProductId = prod.ProductId;
                sale.Quantity = Int32.Parse(Qty);
                sale.SaleCustomer = cust;
                sale.SaleProduct = prod;
                db.CustomerSale.Add(sale);
                db.SaveChanges();
            }
        }
        
    }
}