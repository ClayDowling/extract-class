using Microsoft.EntityFrameworkCore;

namespace extractclass
{
    public class CustomerContext : DbContext
    {

        public DbSet<Customer> Customer { get; set; }
        public DbSet<Product> Product { get; set; }
        
        public DbSet<CustomerSale> CustomerSale { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder options)
                    => options.UseSqlite("Data Source=customers.db");
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerSale>()
                .HasKey(s => new { s.TransactionId, s.CustomerId, s.ProductId });
        }
    }
}